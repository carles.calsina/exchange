<?php

namespace App\Application;


interface Service
{
    public function exec(Request $request): Response;
}
