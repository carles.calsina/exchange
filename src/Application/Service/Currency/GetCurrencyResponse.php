<?php

namespace App\Application\Service\Currency;

use App\Application\Response;
use App\Domain\Currency\Currency;

class GetCurrencyResponse implements Response
{
    /** @var  Currency */
    private $currency;

    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }

    public function currency(): Currency
    {
        return $this->currency;
    }
}
