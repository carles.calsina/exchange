<?php

namespace App\Application\Service\Currency;

use App\Application\Request;

class GetCurrencyRequest implements Request
{
    /** @var  int */
    private $currencyId;

    public function __construct(int $currencyId)
    {
        $this->currencyId = $currencyId;
    }

    public function currencyId(): int
    {
        return $this->currencyId;
    }
}