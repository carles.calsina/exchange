<?php

namespace App\Application\Service\Currency;

use App\Application\Request;
use App\Application\Response;
use App\Application\Service;
use App\Domain\Currency\CurrencyRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetCurrencyService implements Service, MessageHandlerInterface
{
    /** @var  CurrencyRepository */
    private $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }


    public function __invoke(GetCurrencyRequest $request)
    {
        return $this->exec($request);
    }


    /**
     * @param GetCurrencyRequest $request
     * @return GetCurrencyResponse
     */
    public function exec(Request $request): Response
    {
        $currency = $this->currencyRepository->get($request->currencyId());

        return new GetCurrencyResponse($currency);
    }
}