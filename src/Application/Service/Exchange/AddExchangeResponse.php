<?php

namespace App\Application\Service\Exchange;

use App\Application\Response;
use App\Domain\Exchange\Exchange;

class AddExchangeResponse implements Response
{
    /** @var  Exchange */
    private $exchange;

    public function __construct(Exchange $exchange)
    {
        $this->exchange = $exchange;
    }

    public function exchange(): Exchange
    {
        return $this->exchange;
    }
}
