<?php

namespace App\Application\Service\Exchange;

use App\Application\Request;

class AddExchangeRequest implements Request
{
    /** @var  float */
    private $rate;

    /** @var  int */
    private $relatedCurrency;

    /** @var  int */
    private $ownCurrency;

    public function __construct(float $rate, int $relatedCurrency, int $ownCurrency)
    {
        $this->rate = $rate;
        $this->relatedCurrency = $relatedCurrency;
        $this->ownCurrency = $ownCurrency;
    }

    public function rate(): float
    {
        return $this->rate;
    }

    public function relatedCurrency(): int
    {
        return $this->relatedCurrency;
    }

    public function ownCurrency(): int
    {
        return $this->ownCurrency;
    }
}