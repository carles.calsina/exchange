<?php

namespace App\Application\Service\Exchange;

use App\Application\Request;
use App\Application\Response;
use App\Application\Service;
use App\Domain\Currency\CurrencyRepository;
use App\Domain\Currency\Exception\CurrencyNotFoundException;
use App\Domain\Exchange\Exception\ExchangeAlreadyExist;
use App\Domain\Exchange\ExchangeRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddExchangeService implements Service, MessageHandlerInterface
{
    /** @var  ExchangeRepository */
    private $exchangeRepository;

    /** @var  CurrencyRepository */
    private $currencyRepository;

    public function __construct(ExchangeRepository $exchangeRepository, CurrencyRepository $currencyRepository)
    {
        $this->exchangeRepository = $exchangeRepository;
        $this->currencyRepository = $currencyRepository;
    }


    public function __invoke(AddExchangeRequest $request)
    {
        return $this->exec($request);
    }


    /**
     * @param AddExchangeRequest $request
     * @return AddExchangeResponse
     */
    public function exec(Request $request): Response
    {
        $this->assertExistCurrency($request->ownCurrency());
        $this->assertExistCurrency($request->relatedCurrency());
        $this->assertExchangeAlreadyExist($request->ownCurrency(), $request->relatedCurrency());

        $exchange = $this->exchangeRepository->create($request->rate(), $request->relatedCurrency(), $request->ownCurrency());

        return new AddExchangeResponse($exchange);
    }

    /**
     * @throws CurrencyNotFoundException
     */
    public function assertExistCurrency(int $currencyId)
    {
        $currency = $this->currencyRepository->find($currencyId);

        if (!$currency) {
            throw new CurrencyNotFoundException($currencyId);
        }
    }

    /**
     * @throws ExchangeAlreadyExist
     */
    public function assertExchangeAlreadyExist(int $ownCurrency, int $relatedCurrency): void
    {
        $exchangeExist = $this->exchangeRepository->findByCurrencyRelated($ownCurrency, $relatedCurrency);

        if ($exchangeExist) {
            throw new ExchangeAlreadyExist();
        }
    }
}