<?php

namespace App\Infrastructure\Persistence\InMemory\Currency;

use App\Domain\Currency\Currency;
use App\Domain\Currency\CurrencyRepository;
use App\Domain\Currency\Exception\CurrencyNotFoundException;
use App\Domain\Exchange\Exchange;

class CurrencyRepositoryImpl implements CurrencyRepository
{
    const AVAILABLE_CURRENCIES = [
        [
            "id" => 1,
            "name" => "EUR",
            "exchanges" => [
                [
                    "id" => 1,
                    "rate" => 1.02,
                    "relatedCurrency" => 2,
                    "ownCurrency" => 1
                ]
            ]
        ],
        [
            "id" => 2,
            "name" => "USD",
            "exchanges" => [
                [
                    "id" => 2,
                    "rate" => 0.98,
                    "relatedCurrency" => 1,
                    "ownCurrency" => 2
                ]
            ]
        ]
    ];


    public function get(int $id): Currency
    {
        foreach (self::AVAILABLE_CURRENCIES as $currency) {
            if ($currency['id'] === $id) {
                return $this->buildCurrency($currency);
            }
        }

        throw new CurrencyNotFoundException($id);
    }

    public function find(int $id): ?Currency
    {
        foreach (self::AVAILABLE_CURRENCIES as $currency) {
            if ($currency['id'] === $id) {
                return $this->buildCurrency($currency);
            }
        }


        return null;
    }

    private function buildExchanges(array $exchanges): array
    {
        $exchangesEntities = [];

        foreach ($exchanges as $exchange) {
            $exchangesEntities[] = new Exchange(
                $exchange['id'],
                $exchange['rate'],
                $exchange['relatedCurrency'],
                $exchange['ownCurrency']
            );
        }

        return $exchangesEntities;
    }

    /**
     * @param $currency
     * @return Currency
     */
    public function buildCurrency($currency): Currency
    {
        return new Currency(
            $currency['id'],
            $currency['name'],
            $this->buildExchanges($currency['exchanges'])
        );
    }
}