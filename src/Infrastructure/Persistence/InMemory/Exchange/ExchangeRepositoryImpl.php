<?php

namespace App\Infrastructure\Persistence\InMemory\Exchange;

use App\Domain\Currency\Exception\CurrencyNotFoundException;
use App\Domain\Exchange\Exchange;
use App\Domain\Exchange\ExchangeRepository;

class ExchangeRepositoryImpl implements ExchangeRepository
{
    const AVAILABLE_EXCHANGES = [
        [
            "id" => 1,
            "rate" => 1.02,
            "relatedCurrency" => 2,
            "ownCurrency" => 1
        ],
        [
            "id" => 2,
            "rate" => 0.98,
            "relatedCurrency" => 1,
            "ownCurrency" => 2
        ]
    ];


    public function get(int $id): Exchange
    {
        foreach (self::AVAILABLE_EXCHANGES as $exchange) {
            if ($exchange['id'] === $id) {
                return $this->buildExchange($exchange);
            }
        }

        throw new CurrencyNotFoundException($id);
    }


    private function buildExchange(array $exchange): Exchange
    {
        return new Exchange(
            $exchange['id'],
            $exchange['rate'],
            $exchange['relatedCurrency'],
            $exchange['ownCurrency']
        );

    }

    public function findByCurrencyRelated(int $ownCurrency, int $relatedCurrency): ?Exchange
    {
        foreach (self::AVAILABLE_EXCHANGES as $exchange) {
            if ($exchange['relatedCurrency'] === $relatedCurrency
                && $exchange['ownCurrency'] === $ownCurrency) {
                return $this->buildExchange($exchange);
            }
        }

        return null;
    }

    public function create(float $rate, int $relatedCurrency, int $ownCurrency): Exchange
    {
        $exchange = new Exchange($this->getNewId(), $rate, $relatedCurrency, $ownCurrency);

        //TODO: SAVE EXCHANGE IN PERSISTENCE

        return $exchange;
    }

    private function getNewId(): int
    {
        return count(self::AVAILABLE_EXCHANGES) + 1;
    }
}