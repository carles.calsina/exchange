<?php

namespace App\Infrastructure\Symfony\Listeners;

use App\Infrastructure\Json\JsonEncoder;
use App\Infrastructure\Annotations\ApiResponseMapping;
use Doctrine\Common\Annotations\AnnotationException;
use ReflectionException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class JsonResponseOnKernelViewListener
{
    use AnnotationsTrait;

    private $headers = [
        'Content-Type' => 'application/vnd.api+json'
    ];

    /** @var JsonEncoder */
    private $encoder;

    public function __construct(JsonEncoder $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @throws ReflectionException
     * @throws AnnotationException
     */
    public function onKernelView(ViewEvent $event)
    {
        /** @var ?ApiResponseMapping $annotation */
        $annotation = $this->getMethodAnnotation(
            $this->getReflectionMethod($event->getRequest()),
            ApiResponseMapping::class
        );

        if (!$annotation) {
            return false;
        }

        $controllerResult = $event->getControllerResult();

        // Edge case for HTTP STATUS CODE 204 NO CONTENT
        if (empty($controllerResult) && $annotation->httpStatus() === Response::HTTP_NO_CONTENT)
        {
            $event->setResponse(new Response('', $annotation->httpStatus(), $this->headers));
            return true;
        }

        // Otherwise returns data with the given HTTP STATUS CODE
        $exceptionClassName = $annotation->class();
        if (!($controllerResult instanceof $exceptionClassName)) {
            return false;
        }
        $content = $this->encoder->encodeData($controllerResult);
        $event->setResponse(new Response($content, $annotation->httpStatus()));
    }
}