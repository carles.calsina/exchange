<?php

namespace App\Infrastructure\Symfony\Listeners;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

trait AnnotationsTrait
{
    /**
     * @throws AnnotationException
     */
    private function getMethodAnnotation(?ReflectionMethod $reflectionMethod, $annotationName)
    {
        if (!$reflectionMethod) {
            return false;
        }
        $annotationReader = new AnnotationReader();
        return $annotationReader->getMethodAnnotation($reflectionMethod, $annotationName);
    }

    /**
     * @throws AnnotationException
     */
    private function getMethodAnnotations(?ReflectionMethod $reflectionMethod, $annotationName): array
    {
        if($reflectionMethod == null) {
            return [];
        }

        $annotationReader = new AnnotationReader();
        $annotations = $annotationReader->getMethodAnnotations($reflectionMethod);
        return array_filter($annotations, function ($annotation) use ($annotationName) {
            return $annotation instanceof $annotationName;
        });
    }

    /**
     * @throws ReflectionException
     */
    private function getReflectionMethod(Request $request): ?ReflectionMethod
    {
        /** @var ParameterBag $requestMetadata */
        $requestMetadata = $request->attributes->get('_request_metadata', new ParameterBag());
        $requestMetadata->set('relative_path', $request->getPathInfo());
        $request->attributes->set('_request_metadata', $requestMetadata);
        $controller = $request->attributes->get('_controller');
        if (preg_match('#(.+)::([\w]+)#', $controller, $matches)) {
            $class = $matches[1];
            $method = $matches[2];
            return new ReflectionMethod($class, $method);
        }
        return null;
    }
}
