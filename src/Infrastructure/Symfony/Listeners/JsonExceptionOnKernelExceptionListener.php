<?php

namespace App\Infrastructure\Symfony\Listeners;

use App\Infrastructure\Annotations\ExceptionMapping;
use Doctrine\Common\Annotations\AnnotationException;
use Exception;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

class JsonExceptionOnKernelExceptionListener
{
    use AnnotationsTrait;

    const UNKNOWN_CODE = 'UNKNOWN';

    const UNKNOWN_STATUS_CODE = 500;

    const KNOWN_EXCEPTIONS = [
    ];

    /** @var string */
    private $env;

    public function __construct(string $env)
    {
        $this->env = $env;
    }

    /**
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof HandlerFailedException) {
            $exception = $exception->getNestedExceptions()[0];
        }


        list($code, $httpStatus) = $this->getCodeAndHttpStatusForException(
            $exception,
            $this->getReflectionMethod($event->getRequest())
        );

        //TODO: LOG EXCEPTION

        $exceptionData = [
            'code' => $httpStatus,
            'message' => $exception->getMessage(),
            'errors' => [
                'message' => $exception->getMessage(),
                'reason' => $code,
                'detail' => $this->isDevelopmentEnv() ? $exception->getTraceAsString() : ''
            ]
        ];
        $event->setResponse(new Response(json_encode([
            'error' => [$exceptionData],
        ]), $httpStatus, ['Content-Type' => 'application/vnd.api+json']));
    }

    /**
     * @throws AnnotationException
     */
    private function getCodeAndHttpStatusForException(Exception $exception, ?ReflectionMethod $reflectionMethod)
    {
        $code = self::UNKNOWN_CODE;
        $httpStatusCode = self::UNKNOWN_STATUS_CODE;

        foreach (self::KNOWN_EXCEPTIONS as $knownException => $values) {
            if ($exception instanceof $knownException) {
                return $values;
            }
        }

        $annotations = $this->getMethodAnnotations($reflectionMethod, ExceptionMapping::class);

        /** @var ExceptionMapping $annotation */
        foreach ($annotations as $annotation) {
            $exceptionClassName = $annotation->class();
            if ($exception instanceof $exceptionClassName) {
                $code = $annotation->code();
                $httpStatusCode = $annotation->httpStatus();
                break;
            }
        }

        return [$code, $httpStatusCode];
    }

    private function isDevelopmentEnv(): bool
    {
        return $this->env === 'dev' || $this->env === 'staging';
    }
}
