<?php

namespace App\Infrastructure\Symfony\Controller;

use App\Infrastructure\Json\JsonRequestContent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method RequestStack get($id): ;
 */
trait JsonRequestContentTrait
{
    protected function getJsonRequestData(Request $request = null): array
    {
        return $this->getJsonRequestContent($request)->data()
            ? $this->getJsonRequestContent($request)->data()['data']
            : [];
    }

    protected function getJsonRequestContent(Request $request = null): JsonRequestContent
    {
        if ($request == null) {
            $request = $this->get('request_stack')->getCurrentRequest();
        }

        return new JsonRequestContent(json_decode($request->getContent(), true));
    }
}