<?php

namespace App\Infrastructure\Annotations;

/**
 * @Annotation
 */
class ExceptionMapping
{
    /**
     * @var string
     * @Required
     */
    private $class;

    /**
     * @var string
     * @Required
     */
    private $code;

    /**
     * @var int
     * @Required
     */
    private $httpStatus;

    public function __construct(array $data)
    {
        if (isset($data['class'])) {
            $this->class = $data['class'];
        }

        if (isset($data['code'])) {
            $this->code = $data['code'];
        }

        if (isset($data['httpStatus'])) {
            $this->httpStatus = $data['httpStatus'];
        }
    }

    public function class(): string
    {
        return $this->class;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function httpStatus(): int
    {
        return $this->httpStatus;
    }
}