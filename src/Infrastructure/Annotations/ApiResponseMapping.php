<?php

namespace App\Infrastructure\Annotations;

/**
 * @Annotation
 */
class ApiResponseMapping
{
    /**
     * @var string
     * @Required
     */
    private $class;

    /**
     * @var int
     * @Required
     */
    private $httpStatus;

    public function __construct(array $data)
    {
        if (isset($data['class'])) {
            $this->class = $data['class'];
        }

        if (isset($data['httpStatus'])) {
            $this->httpStatus = $data['httpStatus'];
        }
    }

    public function class(): string
    {
        return $this->class;
    }

    public function httpStatus(): int
    {
        return $this->httpStatus;
    }
}