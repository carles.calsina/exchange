<?php

namespace App\Infrastructure\Bus;

use App\Application\Request;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class ServiceBus
{
    use HandleTrait;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function dispatch(Request $request)
    {
        return $this->handle($request);
    }
}
