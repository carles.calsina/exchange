<?php

namespace App\Infrastructure\Controller;

use App\Application\Service\Currency\GetCurrencyRequest;
use App\Application\Service\Currency\GetCurrencyResponse;
use App\Infrastructure\Bus\ServiceBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Domain\Currency\Currency;
use App\Domain\Currency\Exception\CurrencyNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Infrastructure\Annotations\ApiResponseMapping;
use App\Infrastructure\Annotations\ExceptionMapping;


class CurrencyController extends AbstractController
{
    /** @var ServiceBus */
    private $serviceBus;

    public function __construct(ServiceBus $serviceBus)
    {
        $this->serviceBus = $serviceBus;
    }


    /**
     * @Route("/currency/{id}", methods={"GET"})
     * @ApiResponseMapping(class=Currency::class, httpStatus=Response::HTTP_OK)
     * @ExceptionMapping(class=CurrencyNotFoundException::class, code="CURRENCY_NOT_FOUND", httpStatus=Response::HTTP_NOT_FOUND)
     * @param Request $request
     */
    public function getCurrency(int $id)
    {
        /** @var GetCurrencyResponse $response */
        $response = $this->serviceBus->dispatch(
            new GetCurrencyRequest($id)
        );

        return $response->currency();
    }

    /**
     * @Route("/currency", methods={"GET"})
     * @ExceptionMapping(class=\Exception::class, code="NOT_IMPLEMENTED_YET", httpStatus=Response::HTTP_METHOD_NOT_ALLOWED)
     * @param Request $request
     */
    public function listCurrency(Request $request)
    {
        throw new \Exception('Not implemented yet!');
    }
}
