<?php

namespace App\Infrastructure\Controller;

use App\Application\Service\Exchange\AddExchangeRequest;
use App\Application\Service\Exchange\AddExchangeResponse;
use App\Infrastructure\Bus\ServiceBus;
use App\Infrastructure\Symfony\Controller\JsonRequestContentTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Infrastructure\Annotations\ExceptionMapping;
use App\Domain\Exchange\Exchange;
use App\Infrastructure\Annotations\ApiResponseMapping;
use App\Domain\Currency\Exception\CurrencyNotFoundException;
use App\Domain\Exchange\Exception\ExchangeAlreadyExist;


class ExchangeController extends AbstractController
{
    use JsonRequestContentTrait;

    /** @var ServiceBus */
    private $serviceBus;

    public function __construct(ServiceBus $serviceBus)
    {
        $this->serviceBus = $serviceBus;
    }

    /**
     * @Route("/currency/{currencyId}/exchange/{exchangeId}", methods={"GET"})
     * @ExceptionMapping(class=\Exception::class, code="NOT_IMPLEMENTED_YET", httpStatus=Response::HTTP_METHOD_NOT_ALLOWED)
     * @param Request $request
     */
    public function getCurrency(Request $request, int $currencyId, int $exchangeId)
    {
        throw new \Exception('Not implemented yet!');
    }

    /**
     * @Route("/currency/{currencyId}/exchange", methods={"GET"})
     * @ExceptionMapping(class=\Exception::class, code="NOT_IMPLEMENTED_YET", httpStatus=Response::HTTP_METHOD_NOT_ALLOWED)
     * @param Request $request
     */
    public function listCurrencyExchange(Request $request, int $currencyId)
    {
        throw new \Exception('Not implemented yet!');
    }


    /**
     * @Route("/currency/{currencyId}/exchange", methods={"POST"})
     * @ApiResponseMapping(class=Exchange::class, httpStatus=Response::HTTP_OK)
     * @ExceptionMapping(class=CurrencyNotFoundException::class, code="CURRENCY_NOT_FOUND", httpStatus=Response::HTTP_NOT_FOUND)
     * @ExceptionMapping(class=ExchangeAlreadyExist::class, code="EXCHANGE_ALREADY_EXIST", httpStatus=Response::HTTP_CONFLICT)
     * @param Request $request
     */
    public function postCurrencyExchange(Request $request, int $currencyId)
    {
        $data = $this->getJsonRequestData($request);

        /** @var AddExchangeResponse $response */
        $response = $this->serviceBus->dispatch(
            new AddExchangeRequest($data['rate'], $data['relatedCurrency'], $currencyId)
        );

        return $response->exchange();
    }

    /**
     * @Route("/currency/{currencyId}/exchange/{exchangeId}", methods={"PATCH"})
     * @ExceptionMapping(class=\Exception::class, code="NOT_IMPLEMENTED_YET", httpStatus=Response::HTTP_METHOD_NOT_ALLOWED)
     * @param Request $request
     */
    public function patchCurrencyExchange(Request $request, int $currencyId, int $exchangeId)
    {
        throw new \Exception('Not implemented yet!');
    }
}
