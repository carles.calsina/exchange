<?php

namespace App\Infrastructure\Json\Google;

use App\Domain\Entity\Entity;
use InvalidArgumentException;

class JsonGoogleStyleEncoder
{
    /** @var array */
    private $schemas;

    public function __construct(array $schemas)
    {
        $this->schemas = $schemas;
    }

    public static function instance(array $schemas): JsonGoogleStyleEncoder
    {
        return new self($schemas);
    }

    public function encodeData($data): string
    {
        return $this->encodeToJson($data);

    }

    private function encodeToJson($data): string
    {
        if (\is_array($data) === false && \is_object($data) === false && $data !== null) {
            throw new InvalidArgumentException();
        }

        $dataEncoded = [];

        if ($data instanceof Entity) {
            if (array_key_exists(get_class($data), $this->schemas)) {
                $dataEncoded['data'] = (new $this->schemas[get_class($data)])->getAttributes($data);
            }
        }

        $result = json_encode($dataEncoded);

        if (!$result) {
            throw new \Exception();
        }

        return $result;
    }
}
