<?php

namespace App\Infrastructure\Json\Schemas;

use App\Domain\Currency\Currency;
use App\Domain\Exchange\Exchange;
use App\Infrastructure\Json\BaseSchema;

class CurrencySchema implements BaseSchema
{
    /** @param Currency $resource */
    public function getAttributes($resource): iterable
    {
        return [
            'id' => $resource->id(),
            'name' => $resource->name(),
            'exchanges' => $this->buildExchanges($resource->exchanges())
        ];
    }

    /**
     * @param Exchange[] $exchanges
     */
    private function buildExchanges(array $exchanges): array
    {
        $exchangesResponse = [];

        foreach ($exchanges as $exchange) {
            $exchangesResponse[] = [
                'id' => $exchange->id(),
                'rate' => $exchange->rate(),
                'currency_id' => $exchange->relatedCurrency()
            ];
        }

        return $exchangesResponse;
    }
}