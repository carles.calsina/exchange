<?php

namespace App\Infrastructure\Json\Schemas;

use App\Domain\Currency\Currency;
use App\Domain\Exchange\Exception\ExchangeAlreadyExist;
use App\Domain\Exchange\Exchange;
use App\Infrastructure\Json\BaseSchema;

class ExchangeSchema implements BaseSchema
{
    /** @param Exchange $resource */
    public function getAttributes($resource): iterable
    {
        return [
            'id' => $resource->id(),
            'rate' => $resource->rate(),
            'relatedCurrency' => $resource->relatedCurrency(),
            'ownCurrency' => $resource->ownCurrency(),
        ];
    }
}