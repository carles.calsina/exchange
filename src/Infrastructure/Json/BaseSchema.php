<?php

namespace App\Infrastructure\Json;

interface BaseSchema
{
    public function getAttributes($resource): iterable;
}