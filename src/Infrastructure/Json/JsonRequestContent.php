<?php

namespace App\Infrastructure\Json;

class JsonRequestContent
{
    /** @var array */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function data(): array
    {
        return $this->data;
    }
}