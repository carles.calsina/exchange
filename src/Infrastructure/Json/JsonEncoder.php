<?php

namespace App\Infrastructure\Json;

use App\Domain\Currency\Currency;
use App\Domain\Exchange\Exchange;
use App\Infrastructure\Json\Google\JsonGoogleStyleEncoder;
use App\Infrastructure\Json\Schemas\CurrencySchema;
use App\Infrastructure\Json\Schemas\ExchangeSchema;

class JsonEncoder
{
    private $schemas = [
        Currency::class => CurrencySchema::class,
        Exchange::class => ExchangeSchema::class
    ];

    /** @var JsonGoogleStyleEncoder */
    private $encoder;

    public function __construct()
    {
        $this->encoder = JsonGoogleStyleEncoder::instance($this->schemas);
    }

    public function encodeData($data): string
    {
        return $this->encoder->encodeData($data);
    }
}
