<?php

namespace App\Domain\Exchange;

use App\Domain\Entity\Entity;

class Exchange implements Entity
{
    /** @var  int */
    private $id;

    /** @var  float */
    private $rate;

    /** @var  int */
    private $relatedCurrency;

    /** @var int int */
    private $ownCurrency;

    public function __construct(int $id, float $rate, int $relatedCurrency, int $ownCurrency)
    {
        $this->id = $id;
        $this->rate = $rate;
        $this->relatedCurrency = $relatedCurrency;
        $this->ownCurrency = $ownCurrency;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function rate(): float
    {
        return $this->rate;
    }

    public function setRate(float $rate)
    {
        $this->rate = $rate;
    }

    public function relatedCurrency(): int
    {
        return $this->relatedCurrency;
    }

    public function ownCurrency(): int
    {
        return $this->ownCurrency;
    }

    public function setRelatedCurrency(int $relatedCurrency)
    {
        $this->relatedCurrency = $relatedCurrency;
    }

    public function setOwnCurrency(int $ownCurrency)
    {
        $this->ownCurrency = $ownCurrency;
    }

    public static function entityName(): string
    {
        return self::class;
    }
}