<?php

namespace App\Domain\Exchange;


interface ExchangeRepository
{
    public function get(int $id): Exchange;

    public function findByCurrencyRelated(int $ownCurrency, int $relatedCurrency): ?Exchange;

    public function create(float $rate, int $relatedCurrency, int $ownCurrency): Exchange;
}