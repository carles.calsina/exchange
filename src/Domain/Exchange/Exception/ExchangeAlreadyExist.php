<?php

namespace App\Domain\Exchange\Exception;

class ExchangeAlreadyExist extends \Exception
{
    public function __construct()
    {
        parent::__construct('Exchange already exist');
    }
}