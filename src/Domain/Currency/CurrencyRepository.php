<?php

namespace App\Domain\Currency;


interface CurrencyRepository
{
    public function get(int $id): Currency;

    public function find(int $id): ?Currency;
}