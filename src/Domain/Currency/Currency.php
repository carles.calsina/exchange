<?php

namespace App\Domain\Currency;

use App\Domain\Entity\Entity;
use App\Domain\Exchange\Exchange;

class Currency implements Entity
{
    /** @var  int */
    private $id;

    /** @var  string */
    private $name;

    /** @var Exchange[] */
    private $exchanges;

    public function __construct(int $id, string $name, array $exchanges)
    {
        $this->id = $id;
        $this->name = $name;
        $this->exchanges = $exchanges;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function exchanges(): array
    {
        return $this->exchanges;
    }

    public function setExchanges(array $exchanges)
    {
        $this->exchanges = $exchanges;
    }

    public static function entityName(): string
    {
        return self::class;
    }
}