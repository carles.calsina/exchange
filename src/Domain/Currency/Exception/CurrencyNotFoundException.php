<?php

namespace App\Domain\Currency\Exception;

class CurrencyNotFoundException extends \Exception
{
    public function __construct(int $currencyId)
    {
        parent::__construct(
            sprintf(
                'Currency with id %d not found',
                $currencyId
            )
        );
    }
}