<?php

namespace App\Domain\Entity;

interface Entity
{
    public static function entityName() : string;
}
