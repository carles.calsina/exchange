# External Api 

Contents
* [Getting started](#getting-started)
* [Endpoints documentation](#endpoints-documentation)
* [Api standards](#api-standards)
* [Endpoints implemented](#endpoints-implemented)
* [Some future improves](#some-future-improves)
* [Business considerations](#business-considerations)

# Getting started

## Initial Setup ##

### Necessary tools

- Docker
- Docker Compose tool

### Starting up the project ###

Then, all you need to start the containers is:
```
docker-compose up -d
```

Before you run the application yo need to execute this:

```
composer install
```

# Endpoints documentation

You need a Postman app to view the documentation. 

Link to download: https://www.getpostman.com/downloads/

The documentation are in /docs/postman/currencyExchange.postman_collection.json, to view import this file in Postman.

You need to import the environment vars to postman. The file are in: /docs/postman/currencyExchange.postman_environment.json

All methods, response and HTTP codes are defined in this documentation.

# Api standards

This api are designed with the GSON standard. This is a Google simply standard for api rest.

A few documentation are in: https://google.github.io/styleguide/jsoncstyleguide.xml

This api is implemented in PHP 7.3 and Symfony 4.3

# Endpoints implemented

This api are 2 endpoints implemented:

- GET currency: Endpoint to get one currency object

- POST Exchange: Endpoint to create a new Exchange for a specific currency.

Other endpoints are think and prepared but not implemented yet:

- GET currency list: Endpoint to get a list of available currencies

- GET Exchange: Endpoint to get a specific exchange information

- GET Exchange list: Endpoint to get all exchange of specific curency

- PATCH Exchange: Endpoint to update one exchange for specific currency. Now only allow to update rate.

The two endpoints implemented have unit test. 

This test you can execute using this command:

```
bin/phpunit
```

I use the messenger Symfony bus to execute application service. I this case is not really useful but in real environment you can log or make transactional for example and it's very useful.

I create two annotations for the controller to parse de response:

ApiResponseMapping -> This allow to parse the response with schemas and show the desired response

ExceptionMapping -> This capture when has exception and show with correct format like GSON decide.


# Some future improves

Some improves we can do in the future in this api are:

- Finalize unimplemented endpoints (obviously)
- Create a integration test
- Create a End to End test
- Security api to make public access for example
- Make a pagination and filtering in a listing endpoints
- Api versioning
- CI/CD
- make real environment with docker to work in local and production with the same configuration

I think this things are not necessary in the test but I think is important in the real environment and I make clear.

# Business considerations

I design this api with two objects:

Currency -> define a information relative to a currency for example name like "USD" or "EUR"
Exchange -> define a information relative to a change one currency to other currency. This object depends from currency because one exchange without currency don't have sense.

Then all exchange endpoints you need to pass the currency ID to execute this operation.