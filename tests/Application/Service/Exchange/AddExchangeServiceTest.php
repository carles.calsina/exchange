<?php

namespace Tests\Application\Service\Currency;

use App\Application\Service\Currency\GetCurrencyRequest;
use App\Application\Service\Currency\GetCurrencyService;
use App\Application\Service\Exchange\AddExchangeRequest;
use App\Application\Service\Exchange\AddExchangeService;
use App\Domain\Currency\Currency;
use App\Domain\Currency\CurrencyRepository;
use App\Domain\Currency\Exception\CurrencyNotFoundException;
use App\Domain\Exchange\Exception\ExchangeAlreadyExist;
use App\Domain\Exchange\Exchange;
use App\Domain\Exchange\ExchangeRepository;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class AddExchangeServiceTest extends TestCase
{
    const DEFAULT_CURRENCY_ID = 1;

    /** @var  CurrencyRepository|ObjectProphecy */
    private $currencyRepository;

    /** @var  ExchangeRepository|ObjectProphecy */
    private $exchangeRepository;

    protected function setUp()
    {
        $this->currencyRepository = $this->prophesize(CurrencyRepository::class);
        $this->exchangeRepository = $this->prophesize(ExchangeRepository::class);
    }

    protected function tearDown()
    {
        unset($this->currencyRepository);
        unset($this->exchangeRepository);
    }

    /**
     * @test
     */
    public function Should_AddExchange_When_RequestIsValid()
    {
        $request = new AddExchangeRequest(1.02, self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID);

        $expectedCurrency = new Currency(self::DEFAULT_CURRENCY_ID, 'EUR', []);

        $expectedExchange = new Exchange(3, 1.02, self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID);

        $this->currencyRepository->find(self::DEFAULT_CURRENCY_ID)
            ->willReturn($expectedCurrency)
            ->shouldBeCalled();

        $this->exchangeRepository->findByCurrencyRelated(self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID)
            ->willReturn(null)
            ->shouldBeCalled();

        $this->exchangeRepository->create(1.02, self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID)
            ->willReturn($expectedExchange)
            ->shouldBeCalled();


        $service = new AddExchangeService(
            $this->exchangeRepository->reveal(),
            $this->currencyRepository->reveal()
        );

        $result = $service->exec($request);

        $this->assertEquals($expectedExchange, $result->exchange());
    }

    /**
     * @test
     */
    public function Should_ThrowCurrencyNotFound_When_OwnCurrencyDoesNotExist()
    {
        $this->expectException(CurrencyNotFoundException::class);

        $request = new AddExchangeRequest(1.02, self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID);

        $this->currencyRepository->find(self::DEFAULT_CURRENCY_ID)
            ->willThrow(CurrencyNotFoundException::class)
            ->shouldBeCalled();


        $service = new AddExchangeService(
            $this->exchangeRepository->reveal(),
            $this->currencyRepository->reveal()
        );

        $service->exec($request);
    }


    /**
     * @test
     */
    public function Should_ThrowExchangeAlreadyExists_When_ExchangeExists()
    {
        $this->expectException(ExchangeAlreadyExist::class);

        $expectedCurrency = new Currency(self::DEFAULT_CURRENCY_ID, 'EUR', []);

        $request = new AddExchangeRequest(1.02, self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID);

        $this->currencyRepository->find(self::DEFAULT_CURRENCY_ID)
            ->willReturn($expectedCurrency)
            ->shouldBeCalled();

        $this->exchangeRepository->findByCurrencyRelated(self::DEFAULT_CURRENCY_ID, self::DEFAULT_CURRENCY_ID)
            ->willReturn(new Exchange(1,1,1,1))
            ->shouldBeCalled();

        $service = new AddExchangeService(
            $this->exchangeRepository->reveal(),
            $this->currencyRepository->reveal()
        );

        $service->exec($request);
    }
}
