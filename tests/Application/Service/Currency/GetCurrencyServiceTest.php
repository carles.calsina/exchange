<?php

namespace Tests\Application\Service\Currency;

use App\Application\Service\Currency\GetCurrencyRequest;
use App\Application\Service\Currency\GetCurrencyService;
use App\Domain\Currency\Currency;
use App\Domain\Currency\CurrencyRepository;
use App\Domain\Currency\Exception\CurrencyNotFoundException;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class GetCurrencyServiceTest extends TestCase
{
    const DEFAULT_CURRENCY_ID = 1;

    /** @var  CurrencyRepository|ObjectProphecy */
    private $currencyRepository;


    protected function setUp()
    {
        $this->currencyRepository = $this->prophesize(CurrencyRepository::class);
    }

    protected function tearDown()
    {
        unset($this->currencyRepository);
    }

    /**
     * @test
     */
    public function Should_GetCurrency_When_RequestAValidCurrencyId()
    {
        $request = new GetCurrencyRequest(self::DEFAULT_CURRENCY_ID);

        $expectedCurrency = new Currency(self::DEFAULT_CURRENCY_ID, 'EUR', []);

        $this->currencyRepository->get(self::DEFAULT_CURRENCY_ID)
            ->willReturn($expectedCurrency)
            ->shouldBeCalled();

        $service = new GetCurrencyService($this->currencyRepository->reveal());

        $result = $service->exec($request);

        $this->assertEquals($expectedCurrency, $result->currency());
    }

    /**
     * @test
     */
    public function Should_ThrowCurrencyNotFoundException_When_CurrencyIdDoesNotExists()
    {
        $this->expectException(CurrencyNotFoundException::class);

        $request = new GetCurrencyRequest(self::DEFAULT_CURRENCY_ID);

        $this->currencyRepository->get(self::DEFAULT_CURRENCY_ID)
            ->willThrow(new CurrencyNotFoundException(self::DEFAULT_CURRENCY_ID))
            ->shouldBeCalled();

        $service = new GetCurrencyService($this->currencyRepository->reveal());

        $service->exec($request);
    }

}
