CREATE TABLE currency (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE exchange (
  id int(11) NOT NULL AUTO_INCREMENT,
  rate float NOT NULL,
  relatedCurrency int(11) NOT NULL,
  ownCurrency int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (relatedCurrency) REFERENCES currency(id),
  FOREIGN KEY (ownCurrency) REFERENCES currency(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;